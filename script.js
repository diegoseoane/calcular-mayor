const num1 = document.querySelector('#num1');
const num2 = document.querySelector('#num2');
const button = document.querySelector('#button');

button.addEventListener('click', function() {

	const value1 = parseInt(num1.value);
	const value2 = parseInt(num2.value);

	if (value1 > value2) {
		alert("Primer Numero es mayor");
	} else if (value2 > value1){
		alert("Segundo Numero es mayor");
	} else {
		alert("Los numeros son iguales");
	}
});